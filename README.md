## Description

OICT test app

## Installation

```bash
npm install
```

## Start

```bash
npm run start
```

## Development
run with nodemon

```bash
npm run start:dev
```

lint code

```bash
npm run lint
```

## Local testing
```bash
curl --request GET 'http://localhost:9001/health-check'

curl --request GET 'http://localhost:9001/cards/123/info' --header 'x-api-key: very-secret-key'

```

## Swagger docs

```
/docs
```


