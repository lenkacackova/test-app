import App from './app'
import HealthCheckController from './controllers/healthCheck.controller'
import CardsController from './controllers/cards.controller'

const app = new App(
    [
        new HealthCheckController(),
        new CardsController()
    ]
)

app.listen()
