import type { NextFunction, Request, RequestHandler, Response } from 'express'
import config from 'config'

export const authMiddleware: RequestHandler = (req: Request, res: Response, next: NextFunction) => {
    const token = req.header('x-api-key')
    if (!token) return res.status(401).json({ message: 'Unauthorized' })
    if (token !== config.get('secret')) {
        return res.status(401).json({ message: 'Invalid Token' })
    }
    next()
}
