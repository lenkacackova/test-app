import axios from 'axios'
import config from 'config'

export interface CardValidityResponse {
    'validity_start': string,
    'validity_end': string
}

export interface CardStateResponse {
    'state_id': number,
    'state_description': string
}

export class CardsService {
    private externalApi: string = config.get<string>('services.litacka')

    public async getCardValidity(id: string): Promise<CardValidityResponse> {
        const response = await axios.get(`${this.externalApi}/cards/${id}/validity`, {
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        })
        return response.data
    }

    public async getCardState(id: string):Promise<CardStateResponse> {
        const response = await axios.get(`${this.externalApi}/cards/${id}/state`, {
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        })
        return response.data
    }
}
