import { Router, Request, Response } from 'express'
import { Controller } from '../interfaces/controller.interface'

/**
 * @swagger
 * paths:
 *   /health-check:
 *     get:
 *       summary: Returns response if service is live.
 *       description: Always returns 200 if service is running.
 *       responses:
 *         200:
 *           description: Success
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   status:
 *                     type: string
 *                     example: OK
 */

export default class HealthCheckController implements Controller {
    public router = Router()

    constructor() {
        this.initRoutes()
    }

    public initRoutes (): void {
        this.router.get('/health-check', this.healthCheck)
    }

    healthCheck (req: Request, res: Response) {
        return res.json({
            status: 'OK'
        })
    }
}
