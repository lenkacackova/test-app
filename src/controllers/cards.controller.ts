import { Router, Request, Response, NextFunction } from 'express'
import { CardsService } from '../services/cards.service'
import moment from 'moment'
import { Controller } from '../interfaces/controller.interface'
import { authMiddleware } from '../middleware/auth'

/**
 * @swagger
 * components:
 *   securitySchemes:
 *     ApiKeyAuth:
 *       type: apiKey
 *       in: header
 *       name: x-api-key",
 * paths:
 *   /cards/{cardId}/info:
 *     get:
 *       summary: Retrieves information about card.
 *       description: Returns status and validity information about card by id.
 *       parameters:
 *         - in: path
 *           name: userId
 *           required: true
 *           description: ID of the card to get
 *           schema:
 *             type: integer
 *       security:
 *         - ApiKeyAuth: []
 *       responses:
 *         200:
 *           description: Success
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   validity_end:
 *                     type: string
 *                     description: The user ID.
 *                     example: 21.12.2022
 *                   state_description:
 *                     type: string
 *                     description: The user ID.
 *                     example: Active
 *         401:
 *           description: Unauthorized
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   message:
 *                     type: string
 *                     example: Invalid token
 *   components:
 *     securityDefinitions:
 *       ApiKeyAuth:
 *         type: apiKey
 *         in: header
 *         name: x-api-key",
 */

export default class CardsController implements Controller {
    public pathPrefix = '/cards'
    public router = Router()
    private cardsService = new CardsService()

    constructor() {
        this.initRoutes()
    }

    public initRoutes (): void {
        this.router.get(`${this.pathPrefix}/:cardId/info`, authMiddleware, this.getStateAndValidity)
    }

    getStateAndValidity = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id: string = req.params.cardId
            const state = await this.cardsService.getCardState(id)
            const validity = await this.cardsService.getCardValidity(id)
            return res.json({
                validity_end: moment(validity.validity_end).format('DD.M.YYYY'),
                state_description: state.state_description
            })
        } catch (err) {
            next(err)
        }
    }
}
