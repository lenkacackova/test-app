import express from 'express'
import * as bodyParser from 'body-parser'
import { Controller } from './interfaces/controller.interface'
import * as http from 'http'
import config from 'config'
import swaggerJSDoc from 'swagger-jsdoc'
import swaggerUi from 'swagger-ui-express'

class App {
    public app: express.Application
    private port: number = config.get('app.port') || 9001
    private hostname: string = config.get('app.hostname') || 'localhost'

    constructor(controllers: Controller[]) {
        this.app = express()

        this.initializeMiddlewares()
        this.initializeRoutes(controllers)
        this.initializeErrorHandler()
        this.initDocs()
    }

    private initializeMiddlewares(): void {
        this.app.use(bodyParser.json())
    }

    private initializeRoutes(controllers: Controller[]): void {
        controllers.forEach((controller) => {
            this.app.use('/', controller.router)
        })
    }

    private initializeErrorHandler() {
        this.app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.status(err.response.status || 500).send({ message: err.message });
        });

    }

    private initDocs(): void {
        const options: swaggerJSDoc.Options = {
            swaggerDefinition: {
                openapi: '3.0.0',
                info: {
                    title: 'Express API for OICT test app',
                    version: '1.0.0'
                },
                description:
                    'A simple REST API application made with Express.',
                servers: [
                    {
                        url: `http://${this.hostname}:${this.port}`,
                        description: 'Development server',
                    }
                ]
            },
            apis: ["src/**/*.ts", "dist/**/*.js"]
        }

        const swaggerSpec = swaggerJSDoc(options)
        this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
    }

    public listen(): void {
        const server: http.Server = http.createServer(this.app)
        server.listen(this.port, () => {
            console.log(`App is running on ${this.port}`)
        })
    }
}

export default App
